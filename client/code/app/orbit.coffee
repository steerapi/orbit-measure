# ss.rpc 'orbit.initialize', (conf)->
  #default conf to populate the form
# exp_row_hash = {}
ss.event.on 'exp:queued', (exp)->
  #append to the table
  html = HT['alert-alert'].render(message: "Queued #{exp.experiment_id}")
  $(html).hide().appendTo('#flash').show().delay(1000).slideUp ->
    $(@).remove()
  # table = require './table'
  # table.create()
  # exp_row_hash[exp.experiment_id] = table.add exp

ss.event.on 'exp:remove', (exp_id)->  
  html = HT['alert-alert'].render(message: "Removed #{exp_id}")
  $(html).hide().appendTo('#flash').show().delay(1000).slideUp ->
    $(@).remove()
  # table = require './table'
  # table.create()
  
ss.event.on 'exp:ran', (exp)->
  # alert JSON.stringify(exp_result)
  #update row status
  # html = HT['table-row'].render({exp_id: message, time: -> timestamp() })
  html = HT['alert-alert'].render(message: "Finished #{exp.experiment_id}")
  $(html).hide().appendTo('#flash').show().delay(1000).slideUp ->
    $(@).remove()
  table = require './table'
  table.create(0,99)

  # table.update exp, exp_row_hash[exp.experiment_id]
# getExp = _.throttle (cb)->
#   ss.rpc 'orbit.get', (data)->
#     cb data
# , 2000

exports.get = (start, end, cb)->
  #put it in the table
  ss.rpc 'orbit.get', start, end, (data)->
    cb data
    
exports.queue = (params, cb) ->
  ss.rpc 'orbit.queue', params, ->
    cb?()

