exports.setup = (cb)->
  WorkspaceRouter = Backbone.Router.extend(
    routes:
      "experiment/config=:params": "experiment"
    experiment: (config)->
      params = JSON.parse(config)
      cb(params)
  )
  router = new WorkspaceRouter()
  Backbone.history.start()
  return router