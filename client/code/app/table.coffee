sum = (list)->
  avg = 0
  for x in list
    avg += parseFloat(x)
  avg
 
avg = (list)->
  sum(list)/list.length

orbit = require './orbit'
exports.initialize = (exps)->
  $('#table').html( '<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-striped"></table>' );
  rows = []
  calllater = []
  for exp in exps
    row = exports.makeRow exp, (fn)->
      calllater.push fn
    rows.push row
  table = $("#table table").dataTable
    "iDisplayLength": 100
    sDom: "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>"
    sPaginationType: "bootstrap"
    oLanguage:
      sLengthMenu: "_MENU_ records per page"
    aaData: rows,
    aoColumns: [
      {
        sTitle: "Exp"
        sType: "html"
      },
      {
        sTitle: "Link"
        sType: "html"
      },
      {
        sTitle: "DL"
        sType: "html"
      },
      {
        sTitle: "k,m"
        sType: "html"  
      },
      {
        sTitle: "HARQ"
        sType: "html"
      },
      {
        sTitle: "ARQ"
        sType: "html"
      },
      {
        sTitle: "Iperf Load"
        sType: "html"
      },
      {sTitle: "Iperf (Mbps)"},
      {sTitle: "Iperf Jitter (ms)"},
      {sTitle: "Iperf Lost"},
      {
        sTitle: "UFTP Load"
        sType: "html"
      },
      {sTitle: "UFTP (KBps)"},
      {sTitle: "UFTP Time (s)"},
      {sTitle: "UFTP nacks"}  
    ]
  for call in calllater
    call()
  popover()
  
exports.create = (start,end)->
  console.log arguments
  orbit.get start, end, (exps)->
    exports.initialize(exps)

row_from_exp = (exp)->
  row = []
  sextract = (obj,xk)->
    items = []
    for k,v of obj[xk]
      key = k.replace(/_conf/,"")+"-"
      if typeof v == 'object'
        for item in sextract obj[xk], k
          items.push key+item
      else
        items.push key+v
    items
  extract = (obj,xk)->
    items = []
    for k,v of obj[xk]
      items.push
        key:k
        value:v
    html = HT['table-list'].render 
      title: xk
      items: items
    html
  scontent = sextract(exp, "config")
  scontent.push("#{exp.experiment_id}")
  row.push HT['table-config'].render
    id: exp.experiment_id
    scontent: JSON.stringify(scontent)
    # buttons: [
    #   title: "orbit"
    #   content: extract(exp.config, "orbit_conf")
    # ,
    #   title: "phys"
    #   content: extract(exp.config, "phys_conf")
    # ,
    #   title: "harq"
    #   on: (exp.config.harq_conf.enable=="true")
    #   content: extract(exp.config, "harq_conf")
    # ,
    #   title: "arq"
    #   on: (exp.config.arq_conf.enable=="true")
    #   content: extract(exp.config, "arq_conf")
    # ,
    #   title: "nc"
    #   on: (exp.config.nc_conf.enable=="true")
    #   content: extract(exp.config, "nc_conf")
    # ,
    #   title: "iperf"
    #   on: (exp.config.iperf_conf.enable=="true")
    #   content: extract(exp.config, "iperf_conf")
    # ,
    #   title: "uftp"
    #   on: (exp.config.uftp_conf.enable=="true")
    #   content: extract(exp.config, "uftp_conf")
    # ]
  row.push HT['table-hover'].render
    on: true
    id: if exp.link_status then exp.link_status.cinr_db else "N/A"
    scontent: JSON.stringify(sextract(exp, "link_status"))
    content: extract(exp, "link_status")
  row.push HT['table-hover'].render
    id: exp.config.phys_conf.dlprof1
    scontent: JSON.stringify(sextract(exp.config, "phys_conf"))
    content: extract(exp.config, "phys_conf")
  row.push HT['table-hover'].render
    on: (exp.config.nc_conf.enable=="true")
    id: "#{exp.config.nc_conf.k},#{exp.config.nc_conf.m}"
    scontent: JSON.stringify(sextract(exp.config, "nc_conf"))
    content: extract(exp.config, "nc_conf") 
  row.push HT['table-hover'].render
    on: (exp.config.harq_conf.enable=="true")
    id: exp.config.harq_conf.enable
    scontent: JSON.stringify(sextract(exp.config, "harq_conf"))
    content: extract(exp.config, "harq_conf") 
  row.push HT['table-hover'].render
    on: (exp.config.arq_conf.enable=="true")
    id: exp.config.arq_conf.enable
    scontent: JSON.stringify(sextract(exp.config, "arq_conf"))
    content: extract(exp.config, "arq_conf") 
    
  if exp.iperf_result
    iperf = exp.iperf_result
    iperf_bw = 1e-6*avg(_.pluck(iperf, 'bandwidth_bps'))
    iperf_jitter = avg(_.pluck(iperf, 'jitter_ms'))
    iperf_lost = sum(_.pluck(iperf, 'lost'))
    iperf_total = sum(_.pluck(iperf, 'total'))
    iperf_percent = 100.0*iperf_lost/iperf_total
    row.push HT['table-hover'].render
      on: (exp.config.iperf_conf.enable=="true")
      id: exp.config.iperf_conf.b
      scontent: JSON.stringify(sextract(exp.config, "iperf_conf"))
      content: extract(exp.config, "iperf_conf") 
    row.push iperf_bw.toFixed(3)
    row.push iperf_jitter.toFixed(2)
    row.push "#{iperf_lost}/#{iperf_total} (#{iperf_percent.toFixed(2)}%)"
  else
    row.push "N/A"
    row.push "N/A"
    row.push "N/A"
    row.push "N/A"        
  if exp.uftp_result
    row.push HT['table-hover'].render
      on: (exp.config.uftp_conf.enable=="true")
      id: exp.config.uftp_conf.rate_kbps
      scontent: JSON.stringify(sextract(exp.config, "uftp_conf"))
      content: extract(exp.config, "uftp_conf") 
    row.push if exp.uftp_result.bw_kbytes then exp.uftp_result.bw_kbytes.toFixed(2) else "N/A"
    row.push if exp.uftp_result.time_s then exp.uftp_result.time_s.toFixed(2) else "N/A"
    row.push if exp.uftp_result.total_nacks then exp.uftp_result.total_nacks else "N/A"
  else
    row.push "N/A"
    row.push "N/A"
    row.push "N/A"
    row.push "N/A"
  row

popover = _.throttle ->
  $('.pop').popover()
, 1000

exports.makeRow = (exp, later)->
  row = row_from_exp(exp)
  popover()
  later ->
    $("##{exp.experiment_id}-rm").click ->
      $('.pop').popover('hide')
      ss.rpc 'orbit.remove', exp.experiment_id, ->
  row
  
exports.add = (exp)->
  table = $("#table table").dataTable()
  row = row_from_exp(exp)
  ret = table.fnAddData [
    row
  ]
  popover()
  row_num = ret[0]
  $("##{exp.experiment_id}-rm").click ->
    $('.pop').popover('hide')
    ss.rpc 'orbit.remove', exp.experiment_id, ->
  row_num
  
# exports.remove = (row)->
#   $("#table table").dataTable().fnDeleteRow(row)
  # exports.create()

# exports.update = (exp, row_num)->
  # row = row_from_exp(exp)
  # exports.create()
  #$("#table table").dataTable().fnUpdate(row, row_num)
