# This file gets called automatically by SocketStream and must always exist
# Make 'ss' available to all modules and the browser console
window.ss = require('socketstream')
ss.server.on 'disconnect', ->
  console.log('Connection down :-(')
ss.server.on 'reconnect', ->
  console.log('Connection back up :-)')

exports.params = params = 
  orbit:
    host: "steerapi@console.sb4.orbit-lab.org"
  phys:
    bs_tx_power: "0"
    ulprof1: "13"
    dlprof1: "17"
    rf_att: "0"
  harq:
    enable: "false"
    mimoul: "false"
    uldelay: "3"
    dldelay: "1"
    pdusn: "false"
    burst: "17"
    mret: "4"
  arq:
    enable: "false"
    bloksize: "256"
    rtmorx: "1000"
    ackproctime: "0"
    wsize: "1024"
    blockltm: "5000"
    sltmo: "10000"
    rtmotx: "1000"
    rxpurgetmo: "5000"
    dlvrorder: "false"
    txackdelay: "100"
  nc:
    enable: "false"
    k:"1"
    t:"80000000"
    n:"16"
    m:"4"
    p:"1"
    u:"1400"
    i:"1000000000"
    h:"22400"
    sender_if: "eth1"
    receiver_if: "wmx0"
  iperf:
    enable: "true"
    l:"1400"
    b:"10m"
    t:"60"
    i:"0.5"
    sender_if: "eth1"
    receiver_if: "wmx0"
  uftp:
    enable: "true"
    mtu:"1400"
    size_by:"50000000"
    rate_kbps:"10000"
    sender_if: "eth1"
    receiver_if: "wmx0"

ss.server.on 'ready', ->
  # Wait for the DOM to finish loading
  jQuery ->
    # Setup bootstrap plugin
    $('input').tooltip()
    $('.collapse').collapse
      toggle:false

    apply = (params)->
      # console.log params
      for k, v of params
        for xk, xv of v
          if xk=="enable"
            $("""##{k}conf input[name="#{xk}"]""").prop("checked", xv == "true")
          else
            $("""##{k}conf input[name="#{xk}"]""").val(xv)
    
    apply(params)
    
    router = require('./router').setup (p)->
      exports.params = params = p
      apply(p)
    
    
    # $('.accordion-heading a').attr 'style', 'padding-bottom:0px;'
    # require('./orbit')
    # $('.nav a[href="#"]').on "click", ->
    #   $('.nav li').removeClass('active')
    #   $(this).parent().addClass('active')
    #       
    # $('.nav a[href="#Graphs"]').on "click", ->
    #   $('.nav li').removeClass('active')
    #   $(this).parent().addClass('active')

    listen = (id,key)->
      $("#{id} input").change ->
        if $(this).attr("type")=="checkbox"
          if $(this).prop "checked"
            $(this).attr("value", "true")
          else
            $(this).attr("value", "false")
        v = $(this).val()
        # n = parseFloat(v)
        # console.log v
        #         if v.match /[a-z]/
        #           v = v
        #         else if isNaN(n)
        #           v = n
          
        params[key][$(this).attr("name")] = v
        router.navigate "experiment/config=#{JSON.stringify(params)}", replace:true

    listen('#orbitconf', "orbit")
    listen('#harqconf', "harq")
    listen('#arqconf', "arq")
    listen('#physconf', "phys")
    listen('#ncconf', "nc")
    listen('#iperfconf', "iperf")
    listen('#uftpconf', "uftp")
    
    orbit = require('./orbit')
    
    $("#queueBtn").click ->
      console.log params
      orbit.queue params
    table = require './table'
    table.create(0,99)

