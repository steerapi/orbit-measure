test = require "utest"
assert = require "assert"

BaseStation = require "../server/rpc/base"
Utils = require "../server/rpc/utils"

test 'test-server',
  '[Utils] execute remote ssh cmd': ->
    Utils.sshExecCmd "ls", (err,stdout,stderr)->
      assert stdout
  '[BaseStation] getHARQ params': ->
    BaseStation.getHARQ (result)->
      assert result.match(/STATUS/)
  '[BaseStation] getARQ params': ->
    BaseStation.getARQ (result)->
      assert result.match(/STATUS/)
  '[BaseStation] getPower params': ->
    BaseStation.getPower (result)->
      assert result.match(/STATUS/)
  '[BaseStation] getUL params': ->
    BaseStation.getUL (result)->
      assert result.match(/STATUS/)
  '[BaseStation] getDL params': ->
    BaseStation.getDL (result)->
      assert result.match(/STATUS/)
  # '[BaseStation] restart params': ->
  #   base.restart (result)->
  #     console.log result
  #     assert(false)
  #     # assert result.match /Failed/
