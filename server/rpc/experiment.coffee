Utils = require "./utils"
Nodes = require "./nodes"
SSHClient = require "./ssh"
_ = require "underscore"
clc = require "cli-color"

parseIperfRow = (row)->
  items = row.replace?(/\s/,"").split(",")[...-2]
  items = _.filter items, (item)->
    item if item
  count = 0
  type = [parseInt,parseInt,_.identity,parseInt,parseInt,_.identity,parseInt,parseInt, _.identity]
  label = ["timestamp","id","interval","transfer_bytes","bandwidth_bps","jitter_ms","lost","total", "percent"]
  items = _.reduce items, (memo, item)->
    memo[label[count]] = type[count](item)
    count++
    memo
  , {}
  console.log items
  items
  
parseIperf = (data)->
  try
    rows = data.split /\n/
    rows = rows[...-1]
    # avgrow = rows[-1..]
    # rest = rows[...-1]
    # avg = parseIperfRow(avgrow[0])

    # console.log rest
    data = []
    for row in rows
      data.push parseIperfRow(row)
  
    # avg =
    #   transfer_bytes: 0
    #   bandwidth_bps: 0
    #   jitter_ms: 0
    #   lost: 0
    #   total: 0
    #   percent: 0
    #   
    # for x in data
    #   avg.transfer_bytes += x.transfer_bytes
    #   avg.bandwidth_bps += x.bandwidth_bps
    #   avg.jitter_ms += x.jitter_ms
    #   avg.lost += x.lost
    #   avg.total += x.total
    #   avg.percent += x.percent
    # 
    # avg.transfer_bytes = avg.transfer_bytes
    # avg.bandwidth_bps = avg.bandwidth_bps/data.length
    # avg.jitter_ms = avg.jitter_ms/data.length
    # avg.lost = avg.lost
    # avg.total = avg.total
    # avg.percent = avg.percent/data.length

    # raw: data
    # last: avg
    data
  catch err
    []
      
    

parseUFTP = (data)->
  try
    unless data
      return {}
    time_s = data.match /Total elapsed time: (\d+\.\d+) seconds/
    bw_kbytes = data.match /Overall throughput: (\d+\.\d+) KB\/s/
    total_nacks = data.match /NAKs: (\d+)/
    # num_blocks = data.match /Bytes: \d+  Blocks: (\d+)  Sections: \d+/
    #   total = data.match /Received \d+ distinct NAKs for pass/g
    #   total_nacks=0
    #   for x in total
    #     match = x.match /Received (\d+) distinct NAKs for pass/
    #     total_nacks += parseInt(match?[1]) 
  
    bw_kbytes: parseFloat(bw_kbytes[1])
    time_s: parseFloat(time_s[1])
    total_nacks : parseFloat(total_nacks[1])
  catch err
    {}
  # num_blocks: parseFloat(num_blocks?[1])

parseLinkStatus = (data)->
  freq_khz : data.match(/Frequency : (\d+) KHz/)[1]
  signal : data.match(/Signal    : (.+)/)[1]
  rssi_dbm : data.match(/RSSI      : (-?\d+) dBm/)[1]
  cinr_db : data.match(/CINR      : (\d+) dB/)[1]
  avg_tx_power_dbm : data.match(/Avg TX PWR: (-?\d+) dBm/)[1]
  bs_id : data.match(/BS ID     : (.+)/)[1]
  
class Experiment
  constructor: (@host)->

  setupWiMAXLink:(mobile, station, xcb)->
    sshmobile = "ssh root@node#{mobile[0]}-#{mobile[1]}"
    sshstation = "ssh root@node#{station[0]}-#{station[1]}"    
    ssh = new SSHClient(@host)
    ssh.connect()
    ssh.exec "#{sshmobile} wimaxcu dconnect", (err, stdout)->
    ssh.exec "#{sshmobile} 'sleep 60 && wimaxcu scan && wimaxcu connect network 51'", (err, stdout)=>
      unless stdout.match(/Connection successful/i)
        ssh.dconnect()
        xcb "error"
    ssh.exec "#{sshmobile} dhclient wmx0", ->
    ssh.exec "#{sshmobile} route del default gw 10.41.0.1", ->
    ssh.exec "#{sshmobile} route add 10.14.1.#{station[1]} gw 10.41.0.1", ->
    ssh.exec "#{sshstation} route add 10.41.14.#{mobile[1]} gw 10.14.0.1", =>
      ssh.dconnect()
      xcb null
    @

  getLinkStatus:(mobile, cb)->
    sshmobile = "ssh root@node#{mobile[0]}-#{mobile[1]}"
    ssh = new SSHClient(@host)
    ssh.connect()
    ssh.exec "#{sshmobile} wimaxcu status link", (err, stdout)=>
      ssh.dconnect()
      try
        cb null, parseLinkStatus(stdout)
      catch err
        cb err
    @
  getIP: (id, interface, cb)->
    sshnode = "ssh root@node#{id[0]}-#{id[1]}"
    ssh = new SSHClient(@host)
    ssh.connect()
    ssh.exec "#{sshnode} ifconfig #{interface}", (err,stdout)=>
      ssh.dconnect()
      match = stdout.match /inet addr:(\d+\.\d+\.\d+\.\d+)  /
      cb null, match?[1]
      
  setupDecoder:(node, encoderip, cb)->
    node = "ssh root@node#{node[0]}-#{node[1]}"
    ssh = new SSHClient(@host)
    ssh.connect()
    ssh.exec "#{node} 'apt-get update && apt-get install -y libnetfilter-queue-dev cmake git'", (err,stdout) ->
    ssh.exec "#{node} git clone https://bitbucket.org/steerapi/amazingnc.git",->
    ssh.exec "#{node} 'cd amazingnc && git stash && git pull && cmake . && make'", ->
    ssh.exec "#{node} killall ncdecoder", ->
    ssh.exec "#{node} iptables -F", ->
    ssh.exec "#{node} iptables -A INPUT -p 252 -s #{encoderip} -j NFQUEUE --queue-num 0", ->
      ssh.dconnect()
      cb null

  setupEncoder:(node, decoderip, cb)->
    node = "ssh root@node#{node[0]}-#{node[1]}"
    ssh = new SSHClient(@host)
    ssh.connect()
    ssh.exec "#{node} 'apt-get update && apt-get install -y libnetfilter-queue-dev cmake git'", (err,stdout) ->
    ssh.exec "#{node} git clone https://bitbucket.org/steerapi/amazingnc.git",->
    ssh.exec "#{node} 'cd amazingnc && git stash && git pull && cmake . && make'", ->
    ssh.exec "#{node} killall ncencoder", ->
    ssh.exec "#{node} iptables -F", ->
    ssh.exec "#{node} iptables -A OUTPUT ! -p 252 -d #{decoderip} -j NFQUEUE --queue-num 1", ->
    ssh.exec "#{node} iptables -A INPUT -s #{decoderip} -p 252 -j NFQUEUE --queue-num 0", ->
      ssh.dconnect()
      cb null
   
  setupNC:(encodernode, decodernode, encoderif, decoderif, cb)->
    @getIP encodernode, encoderif, (err, encoderip) =>
      @getIP decodernode, decoderif, (err, decoderip) =>
        @setupEncoder encodernode, decoderip, =>
          @setupDecoder decodernode, encoderip, =>
            cb null
    @
  runEncoder: (node,encoderip, decoderip, args, cb)->
    node = "ssh root@node#{node[0]}-#{node[1]}"
    ssh = new SSHClient(@host)
    ssh.connect()
    console.log args
    ssh.exec "#{node} killall ncencoder & killall ncdecoder", ->
    ssh.exec "#{node} ./amazingnc/ncencoder/ncencoder --eip=#{encoderip} --dip=#{decoderip} #{args} & echo ", ->
    ssh.exec "#{node} echo", ->
      ssh.dconnect()
      cb null
  runDecoder: (node,encoderip, decoderip, p, cb)->  
    node = "ssh root@node#{node[0]}-#{node[1]}"
    ssh = new SSHClient(@host)
    ssh.connect()
    ssh.exec "#{node} killall ncencoder & killall ncdecoder", ->
    ssh.exec "#{node} ./amazingnc/ncdecoder/ncdecoder --eip=#{encoderip} --dip=#{decoderip} --p=#{p}& echo ", ->
    ssh.exec "#{node} echo", ->
      ssh.dconnect()
      cb null
     
  runNC: (encodernode, decodernode, encoderif, decoderif, params, cb)->
    @getIP encodernode, encoderif, (err, encoderip) =>
      @getIP decodernode, decoderif, (err, decoderip) =>
        delete params["enable"]
        delete params["receiver_if"]
        delete params["sender_if"]
        args = ""
        for k,v of params
          args += "--#{k}=#{v} "
        @runDecoder decodernode, encoderip, decoderip, params.p, =>
          @runEncoder encodernode, encoderip, decoderip, args, =>
            cb null
    @
  setupIperf:(node, cb)->
    sshnode = "ssh root@node#{node[0]}-#{node[1]}"    
    ssh = new SSHClient(@host)
    ssh.connect()
    ssh.exec "#{sshnode} apt-get install -y iperf", (err,stdout)->
      ssh.dconnect()
      cb()
      
  runIperfSender:(node, recieverip, params, cb)->
    sshnode = "ssh root@node#{node[0]}-#{node[1]}"    
    ssh = new SSHClient(@host)
    ssh.connect()
    console.log clc.red(recieverip)
    ssh.exec "#{sshnode} killall iperf", (err,stdout)=>
    ssh.exec "#{sshnode} 'sleep 1 && iperf -c #{recieverip} -ul #{params.l} -b #{params.b} -t #{params.t} -i #{params.i} -x CMSV -y c'", (err,stdout)=>
      ssh.dconnect()
      cb()
  runIperfReciever:(node, params, cb, xcb)->
    sshnode = "ssh root@node#{node[0]}-#{node[1]}"    
    ssh = new SSHClient(@host)
    ssh.connect()
    ssh.exec "#{sshnode} killall iperf", (err,stdout)=>
      ssh.dconnect()
      Utils = require "./utils"
      server = Utils.sshExecCmd "ssh #{@host} #{sshnode} 'iperf -usl #{params.l} -i #{params.i} -x CMSV -y c'", parseInt(params.t)+10, (err,stdout, stderr) ->
        xcb(null, stdout)
      cb(null, server)

  runIperf:(sendernode, recievernode, senderif, receiverif, params, nc, cb)->
    @getIP sendernode, senderif, (err, senderip) =>
      @getIP recievernode, receiverif, (err, receiverip) =>
        @setupIperf sendernode, =>
          @setupIperf recievernode, =>          
            nc? =>
              @runIperfReciever recievernode, params, (err,server)=>
                @runIperfSender sendernode, receiverip, params, =>
                  console.log "SENDER DONE"
                  setTimeout ->
                    server.kill()
                  ,10000
              , (err, stdout)=>
                result = parseIperf(stdout)
                # _.extend result, 
                #                   log : stdout
                cb(null, result)
    @
  setupUFTP: (node, cb)->
    sshnode = "ssh root@node#{node[0]}-#{node[1]}"    
    ssh = new SSHClient(@host)
    ssh.connect()
    ssh.exec "#{sshnode} killall uftpd", (err,stdout)=>
    ssh.exec "#{sshnode} killall uftp", (err,stdout)=>
    ssh.exec "#{sshnode} 'wget http://www.tcnj.edu/~bush/downloads/uftp-3.6.1.tar && tar -xvf uftp-3.6.1.tar && rm uftp-3.6.1.tar && cd uftp-3.6.1/ && make && make install'", (err,stdout)->
      ssh.dconnect()
      cb()
      
  runUFTPReciever: (node, senderip, params, cb)->
    sshnode = "ssh root@node#{node[0]}-#{node[1]}"    
    ssh = new SSHClient(@host)
    ssh.connect()
    ssh.exec "#{sshnode} killall uftpd", (err,stdout)=>
    ssh.exec "#{sshnode} uftpd -H #{senderip} -B #{params.size_by} -D /tmp/", (err,stdout)=>
      ssh.dconnect()
      cb()
    
  runUFTPSender: (node, recieverip, params, cb)->
    sshnode = "ssh root@node#{node[0]}-#{node[1]}"    
    ssh = new SSHClient(@host)
    ssh.connect()
    ssh.exec "#{sshnode} killall uftp", (err,stdout)=>
    ssh.exec "#{sshnode} dd if=/dev/urandom of=output.dat bs=#{params.size_by} count=1",->
    ssh.exec "#{sshnode} uftp -S 240 -H #{recieverip} output.dat -R #{params.rate_kbps} -U -T -b #{params.mtu} -m 60 -W 1000%", (err,stdout)=>
      ssh.dconnect()
      cb(null, stdout)
    
  runUFTP:(sendernode, recievernode, senderif, receiverif ,params, nc, cb)->
    @getIP sendernode, senderif, (err, senderip) =>
      @getIP recievernode, receiverif, (err, receiverip) =>
        @setupUFTP sendernode, =>
          @setupUFTP recievernode, =>    
            nc? =>
              @runUFTPReciever recievernode, senderip, params, =>
                @runUFTPSender sendernode, receiverip, params, (err,stdout)=>
                  console.log clc.red(stdout)
                  result = parseUFTP(stdout)
                  _.extend result, 
                    log : stdout
                  cb(null, result)
    @

  killNC:(node, cb)->
    node = "ssh root@node#{node[0]}-#{node[1]}"    
    ssh = new SSHClient(@host)
    ssh.connect()
    ssh.exec "#{node} killall ncdecoder", ->
    ssh.exec "#{node} iptables -F", ->
    ssh.exec "#{node} killall ncencoder", ->
      ssh.dconnect()
      cb()         
    @
  killIperf:(node, cb)->
    node = "ssh root@node#{node[0]}-#{node[1]}"    
    ssh = new SSHClient(@host)
    ssh.connect()
    ssh.exec "#{node} killall iperf", ->
      ssh.dconnect()
      cb()  
    @
  killUFTP:(node, cb)->
    node = "ssh root@node#{node[0]}-#{node[1]}"    
    ssh = new SSHClient(@host)
    ssh.connect()
    ssh.exec "#{node} killall uftpd", ->
    ssh.exec "#{node} killall uftp", ->
      ssh.dconnect()
      cb()  
    @       

module.exports = Experiment

# exp = new Experiment("steerapi@console.sb4.orbit-lab.org")
# exp.connect()
# exp.killNC 8, 9, ->
#   console.log arguments

# exp = new Experiment("steerapi@console.sb6.orbit-lab.org")
# exp.runIperf [1,1], [1,2], "eth1", "eth1" 
#   i: 0.5
#   l: 1400
#   t: 10
#   b: 4000000
# , ->
#   console.log arguments

# exp = new Experiment("steerapi@console.sb6.orbit-lab.org")
# exp.runUFTP [1,1], [1,2], "eth1", "eth1" 
#   size_by: 10000000
#   rate_kbps: 5000000
#   mtu: 1400
# , ->
#   console.log arguments
  
# exp = new Experiment("steerapi@console.sb4.orbit-lab.org")
# exp.setupWiMAXLink(
#   8, 9, ->
# )

# exp = new Experiment("steerapi@console.sb6.orbit-lab.org")
# exp.getLinkStatus(
#   [1,2], (err, link_status)->
#     console.log arguments
# )

# exp = new Experiment("steerapi@console.sb6.orbit-lab.org")
# exp.setupNC [1,2], [1,1], "eth1", "eth1", (err ,enip, deip)->
# exp.runNC [1,2], [1,1], "eth1", "eth1", 
#   k:1
#   t:8000000
#   n:16
#   m:2
#   x:1
#   sl:16*1400
#   tl:80000000
# , ->
#   console.log arguments
# exp.connect()
# exp.setupNC [1,1], [1,2],
#   enable: "true"
#   k:1
#   t:8000000
#   n:16
#   m:2
#   x:1
#   sl:16*1400
#   tl:80000000
#   sender_if: "eth1"
#   receiver_if: "eth1"
# ,->
#   console.log arguments
#   

# exp = new Experiment("steerapi@console.sb6.orbit-lab.org")
# exp.connect()
# exp.runUFTP [1,1], [1,2], 
#   size_by: 10000000
#   rate_bps: 5000000
#   mtu: 1400
#   sender_if: "eth1"
#   receiver_if: "eth1"
# , ->
#   console.log arguments
# 

