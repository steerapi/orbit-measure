clc = require("cli-color")
exec = require("child_process").exec

class Utils
  @sshExecCmd = (cmd, to, cb)->
    if typeof(to) == 'function'
      cb = to
      to = 0
    console.log clc.blue("Executing #{cmd}")
    child = exec("#{cmd}", timeout:to*1000, (error, stdout, stderr) ->
      console.log clc.green(stdout)
      console.log clc.red(stderr)
      console.log clc.yellow(error) if error isnt null
      cb(error, stdout, stderr)
    )
  @parseArgs = (params)->
    args = ""
    for k,v of params
      args += "#{k}=#{v}&"
    return args
  @asyncTillTrue = (exec,done)->
    handler = (v)->
      if v==false
        exec handler
      else
        done()
    exec handler
    
module.exports = Utils