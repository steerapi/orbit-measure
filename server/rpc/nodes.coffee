SSHSession = require './session'

class Nodes extends SSHSession
  lookForExistingWiMAXNodes: (n, xcb)->
    ssh = @ssh
    ssh.exec "omf_tell on all", (err, stdout)->
      match = stdout.match(/n_\d+_\d+/g)
      if match
        ijs = []
        for x in match
          m = x.match /n_(\d+)_(\d+)/
          ijs.push [m[1],m[2]]
        max = ijs.length
        console.log ijs
        nodes = []
        handler = (ij,cb)->
          ssh.exec "ssh -o \"StrictHostKeyChecking no\" root@node#{ij[0]}-#{ij[1]} ls", (err, stdout)->
            if stdout.match "i2400m-fw-1.5.0"
              nodes.push ij
            if nodes.length >= n
              xcb null, nodes
            else if ijs.length>0
              cb ijs.shift(), cb
            else
              xcb null, null
        handler(ijs.shift(), handler)
    @

  lookForNodes:(n, xcb)->
    ssh = @ssh
    ssh.exec "omf_tell on all", (err, stdout)->
      match = stdout.match(/n_\d_\d/g)
      if match
        ijs = []
        for x in match
          m = x.match /n_(\d+)_(\d+)/
          ijs.push [m[1],m[2]]
        console.log ijs
        max = ijs.length
        nodes = []
        handler = (i,cb)->
          ij = ijs[i]
          ssh.exec "ssh -o \"StrictHostKeyChecking no\" root@node#{ij[0]}-#{ij[1]} ls", (err, stdout)->
            if stdout.match "i2400m-fw-1.5.0"
              nodes.push ij
            if nodes.length >= n
              xcb null, nodes
            else
              next = ((i+1)%max)
              handler next, handler
        handler(0, handler)
    @
    
  setupNodes:(n, xcb)->
    # @ssh.exec "omf_load all wimax-dev.ndz", ->
    that = @
    ssh = @ssh
    that.lookForExistingWiMAXNodes 2, (err,result)->
      if result==null
        ssh.exec "omf_load all wimax-dev.ndz", ->
        ssh.exec "omf_tell on all", ->
          that.lookForNodes n,(err, nodes)->
            xcb err, nodes
        @
      else
        xcb err, result

module.exports = Nodes

# nodes = new Nodes("steerapi@console.sb4.orbit-lab.org")
# nodes.connect ->
#   nodes.dconnect()
#   console.log arguments
# # nodes.lookForExistingWiMAXNodes 2, (err,result)->
# #   console.log arguments
# # nodes.lookForNodes 2, (err,result)->
# #   console.log arguments
# nodes.setupNodes 2, (err,result)->
#   console.log arguments