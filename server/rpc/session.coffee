SSHClient = require './ssh'

class SSHSession
  constructor: (@host)->
    @log = ""
    @err = ""
  connect: (timeout, err_cb)->
    @ssh = new SSHClient(@host)
    if typeof timeout is "function"
      err_cb = timeout
      timeout = undefined
    if timeout
      setTimeout =>
        @dconnect()
      , timeout
    @ssh.connect(err_cb)
    @
  dconnect: ->
    @log += @ssh.log
    @err += @ssh.err
    @ssh.dconnect()
    @ssh = null  
  end: (cb)->
    ssh = @ssh
    ssh.exec "echo end", (err, stdout) ->
      cb()
      
module.exports = SSHSession