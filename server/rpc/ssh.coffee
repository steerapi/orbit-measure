Expect = require 'node-expect'
spawn = require('child_process').spawn
EventEmitter = require('events').EventEmitter
clc = require("cli-color")

class SSHClient extends EventEmitter
  constructor:(@addr)->        
    @log = ""
    @err = ""
  connect:(err_cb)->
    @ssh = spawn('ssh',["-t","-t",@addr])
    @ssh.stdout.on 'data', (data)=>
      @log+=data+""
      console.log clc.green(""+data)
    @ssh.stderr.on 'data', (data)=>
      @err+=data+""
      err_cb? data+""
      console.log clc.red(""+data)        
    @ssh.on 'exit', (data)=>
      @ssh = undefined
      @emit 'exit', data
    @parser = new Expect
      on:(event,listener)=>
        @ssh.stdout.on event, listener
        @ssh.stderr.on event,listener
      write:(x)=>
        @ssh.stdin.write x
  dconnect:->
    @ssh.kill()
  exec:(cmd, cb)->
    if @ssh
      @parser = @parser.conversation(/\$/).sync().expect().send("#{cmd} && echo\n").expect(/\$/).handler(->
        cb null, arguments[0].input.split("\r\n")[1...-1].join("\r\n")
      ).send("\n").end()
    else
      cb "not connect"

module.exports = SSHClient