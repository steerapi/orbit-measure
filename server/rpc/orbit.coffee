# Server-side Code
_ = require "underscore"
async = require "async"
clc = require "cli-color"

DBUtils = require "./dbutils"
Nodes = require("./nodes")
Base = require "./base"
Experiment = require "./experiment"
taskQueue = async.queue (task,cb)->
  task(cb)
, 1

setupNodes = (params, cb) ->
  host = "#{params.orbit.host}"
  N = new Nodes(host)
  N.connect().setupNodes 2, (err, nodes)->
    N.dconnect()
    cb(err, nodes)

setBSParams = (mobile, station, params, xcb) ->
  precmd = "#{params.orbit.host}"
  base = new Base(precmd)
  changed = false
  rebootH = (err, c)->
    changed = changed or c
  endH = (err, c)->
    changed = changed or c
    if changed == true
      base.restart ->
        x = false
        async.until ->
          x
        , (cb)->
          base.checkBSUp (err, c)->
            x=c
            cb(null)
        , ->
          base.dconnect()
          xcb(null)          
    else
      xcb(null)
  base.connect().setHARQ(
    params.harq,rebootH
  ).setARQ(
    params.arq,rebootH
  ).setPower(
    params.phys.bs_tx_power,rebootH
  ).setUL(
    params.phys.ulprof1, rebootH
  ).setDL(
    params.phys.dlprof1, endH
  )
  # .setRFAtt(
  #     mobile, params.phys.rf_att, endH
  #   )

runExperiment = (mobile, station, params, xcb)->
  precmd = "#{params.orbit.host}"
  exp_result = {}
  exp = new Experiment(precmd)
  
  x = (cb)->
    exp.killNC mobile, ->
      exp.killNC station, ->
        cb()
  y = (cb)->
    exp.killIperf mobile, ->
      exp.killIperf station, ->
        cb()
  z = (cb)->
    exp.killUFTP mobile, ->
      exp.killUFTP station, ->
        cb()

  nc = (cb)->
    cb()

  if params.nc.enable=="true"
    x = (cb)->
      exp.setupNC station, mobile, params.nc.sender_if, params.nc.receiver_if, ->
        # cb()
    # nc = (cb)->
      # console.log clc.red("GET HERE")
        exp.runNC station, mobile, params.nc.sender_if, params.nc.receiver_if, params.nc, ->
          cb()

  if params.iperf.enable=="true"
    y = (cb)->
      exp.runIperf station, mobile, params.iperf.sender_if, params.iperf.receiver_if, params.iperf, nc, (err,iperf)->
        exp_result["iperf_result"] = iperf
        cb()
  if params.uftp.enable=="true"
    z = (cb)->
      exp.runUFTP station, mobile, params.uftp.sender_if, params.uftp.receiver_if, params.uftp, nc, (err,uftp)->
        exp_result["uftp_result"] = uftp
        cb()

  exp.setupWiMAXLink mobile, station, ->
    exp.getLinkStatus mobile, (err, link_status)->
      exp_result["stdlog"] = "Not Implemented"
      exp_result["stderr"] = "Not Implemented"      
      if err
        xcb(err, exp_result)
      else
        exp_result["link_status"] = link_status
        x ->
          y ->
            z ->
              xcb(null, exp_result)

db = new DBUtils
  urlRoot: "http://api.mob1.stackmob.com/"
  consumerKey: "a98bb1a2-13ae-4ff0-9ec8-16b41cd36e59"
  consumerSecret: "2462c3ac-af0f-44ac-b63e-9064092bedc7"
  appName: "orbit"
  version: 0
  dev: true 
exports.actions = (req, res, ss) ->
  remove: (exp_id)->
    db.remove "experiment", experiment_id:exp_id, ->
      ss.publish.all('exp:remove', exp_id)
      res("ok")
  get: (start, end)->
    db.getExperiments start, end, (err, data)->
      res(data)
  queue: (params)->
    res("ok got it")
    db.saveConfig params, (err, config_id, params)->
      db.saveExperiment config: config_id, (err, experiment)->
        exp_id = experiment["experiment_id"]
        ss.publish.all('exp:queued', experiment)    
        taskQueue.push (queue_cb)->
          try
            setupNodes params, (err, nodes)->
              mobile = [1,7]#nodes[0]
              station = [1,2]#nodes[1]
              setBSParams mobile,station, params, ->
                runExperiment mobile, station, params, (err,exp_result)->
                  promise = require "node-promise"
                  piperf = new promise.Promise()
                  puftp = new promise.Promise()
                  plink = new promise.Promise()
                  iperf_id = null
                  uftp_id = null
                  link_id = null
                
                  if exp_result.iperf_result
                    db.saveIperfResult exp_result.iperf_result, (err,iperf_results)->
                      iperf_id = iperf_results
                      piperf.resolve()
                  else
                    piperf.resolve()
                  if exp_result.uftp_result
                    db.create "uftp_result", exp_result.uftp_result, (err,uftp_result)->
                      uftp_id = uftp_result.uftp_result_id
                      puftp.resolve()
                  else
                    puftp.resolve()
                  if exp_result.link_status
                    db.create "link_status", exp_result.link_status, (err,link_status)->
                      link_id = link_status.link_status_id
                      plink.resolve()
                  else
                    plink.resolve()
              
                  promise.all([piperf,puftp,plink]).then ->
                    _.extend exp_result, 
                      "experiment_id": exp_id

                    exp_result.iperf_result =  iperf_id
                    exp_result.uftp_result = uftp_id
                    exp_result.link_status = link_id
                
                    db.updateExperiment exp_result, (err, result)->
                      ss.publish.all('exp:ran', result)
                      queue_cb()
                      console.log arguments
                    
          catch err
            queue_cb()
            console.log clc.red("ERROR #{err}")

process.on 'uncaughtException', (exception)->
  console.log clc.red("EXCEPTION #{exception}")
