# StackMob = require("stackmob-nodejs")
# StackMob.init
#   urlRoot: "http://api.mob1.stackmob.com/"
#   consumerKey: "a98bb1a2-13ae-4ff0-9ec8-16b41cd36e59"
#   consumerSecret: "2462c3ac-af0f-44ac-b63e-9064092bedc7"
#   appName: "orbit"
#   version: 0
#   dev: true
# 
# HarqConf = StackMob.Model.extend(schemaName: "harq_conf")
# ArqConf = StackMob.Model.extend(schemaName: "arq_conf")
# PhysConf = StackMob.Model.extend(schemaName: "phys_conf")
# OrbitConf = StackMob.Model.extend(schemaName: "orbit_conf")
# UftpConf = StackMob.Model.extend(schemaName: "uftp_conf")
# IperfConf = StackMob.Model.extend(schemaName: "iperf_conf")
# NcConf = StackMob.Model.extend(schemaName: "nc_conf")
# 
# Config = StackMob.Model.extend(schemaName: "config")
# 
# UftpResult = StackMob.Model.extend(schemaName: "uftp_result")
# IperfResult = StackMob.Model.extend(schemaName: "iperf_result")
# IperfRow = StackMob.Model.extend(schemaName: "iperf_row")
# 
# Experiment = StackMob.Model.extend(schemaName: "experiment")
# 
# LinkStatus =  StackMob.Model.extend(schemaName: "link_status")
# 
# HarqConfs = StackMob.Collection.extend(model: HarqConf)
# ArqConfs = StackMob.Collection.extend(model: ArqConf)
# PhysConfs = StackMob.Collection.extend(model: PhysConf)
# OrbitConfs = StackMob.Collection.extend(model: OrbitConf)
# UftpConfs = StackMob.Collection.extend(model: UftpConf)
# IperfConfs = StackMob.Collection.extend(model: IperfConf)
# NcConfs = StackMob.Collection.extend(model: NcConf)
# 
# Configs = StackMob.Collection.extend(model: Config)
# 
# UftpResults = StackMob.Collection.extend(model: UftpResult)
# IperfResults = StackMob.Collection.extend(model: IperfResult)
# IperfRows = StackMob.Collection.extend(model: IperfRow)
# 
# Experiments = StackMob.Collection.extend(model: Experiment)
# 
# LinkStatuses =  StackMob.Collection.extend(model: LinkStatus)

# Hello = StackMob.Model.extend(schemaName: "hello")
# hello = new Hello(sd:"sd")
# hello.create
#   success: (model)->
#     console.log model.toJSON()

class DBUtils
  constructor: (params)->
    @StackMob = require("stackmob-nodejs")
    @StackMob.init params
      # urlRoot: "http://api.mob1.stackmob.com/"
      # consumerKey: "a98bb1a2-13ae-4ff0-9ec8-16b41cd36e59"
      # consumerSecret: "2462c3ac-af0f-44ac-b63e-9064092bedc7"
      # appName: "orbit"
      # version: 0
      # dev: true
  remove:(schemaName, data, cb)->
    StackMob = @StackMob
    Model =  StackMob.Model.extend(schemaName: schemaName)
    model = new Model(data)
    model.destroy
      success: (model)->
        cb null, model
      error: (model, response)->
        cb response
        
  create:(schemaName, data, cb)->
    StackMob = @StackMob
    Model =  StackMob.Model.extend(schemaName: schemaName)
    Collection =  StackMob.Collection.extend(model: Model)
    query = new StackMob.Collection.Query()
    for k,v of data
      query.equals(k,v)  
    create = ->
      model = new Model(data)
      model.create
        success:(model)->
          result = model.toJSON()
          cb null, result
        error:(model, response)-> 
          cb response 
    create()
    # collection = new Collection()
    # collection.query query, 
    #   success:(collection)->
    #     result = collection.toJSON()
    #     if result.length > 0
    #       cb null, result[0]
    #     else
    #       create()
    #   error: (model, response)->
    #     create()
  saveIperfResult: (iperfs, cb)->
    promise = require "node-promise"
    ds = []
    ps = []
    iperfs.forEach (iperf)=>
      if iperf.percent == 0
        iperf.percent = 1e-20
      if iperf.jitter_ms == 0
        iperf.jitter_ms = 1e-20
      p = new promise.Promise()
      ps.push p
      @create "iperf_result", iperf, (err, data)->
        console.log arguments
        ds.push data["iperf_result_id"]
        p.resolve()
    promise.all(ps).then ->
      cb(null, ds)
        
  saveConfig: (xconfigs, cb)->
    that = @
    saveConfigInner = (configs)->
      promise = require "node-promise"
      ids = {}
      ps = []
      for k,v of configs
        scn = "#{k}_conf"
        p = new promise.Promise()
        ps.push(p)
        console.log scn
        console.log v
        do (p,v,scn)->
          that.create scn, v, (err, data)->
            console.log scn
            console.log "created"
            console.log arguments
            ids[scn] = data["#{scn}_id"]
            p.resolve()
      console.log ids
      promise.all(ps).then ->
        that.create "config", ids, (err,data)->
          console.log arguments
          id = data["config_id"]
          cb(null,id,configs)
      ,->
        cb "error"
    nconfs = [xconfigs]   
    for k,v of xconfigs
      for vk, vv of v
        nnconfs = []
        x = vv.split ","
        for xv in x
          for conf in nconfs
            nconfigs = JSON.parse(JSON.stringify(conf))
            nconfigs[k][vk] = ""+xv
            nnconfs.push nconfigs
        nconfs = nnconfs
    # console.log _.pluck(nconfs, "iperf")
    for nconf in nconfs
      saveConfigInner nconf, cb

  saveExperiment: (params, cb)->
    StackMob = @StackMob
    Model =  StackMob.Model.extend(schemaName: "experiment")
    model = new Model(params)
    model.create
      success: (model)->
        result = model.toJSON()
        cb null,result
      error: (model, response)->
        cb response
    #return experiment model
  
  updateExperiment: (params, cb)->
    StackMob = @StackMob
    Model =  StackMob.Model.extend(schemaName: "experiment")
    model = new Model(params)
    model.save {},
      success: (model)->
        result = model.toJSON()
        id = result["experiment_id"]
        cb null,result
      error: (model, response)->
        cb response
  getExperiments: (start=0, end=99, cb)->
    StackMob = @StackMob
    Model =  StackMob.Model.extend(schemaName: "experiment")
    Collection =  StackMob.Collection.extend(model: Model)
    collection = new Collection()
    query = new StackMob.Collection.Query()
    query.setExpand(2)
    query.setRange(start,end)
    query.orderDesc("createddate")
    collection.query query,
      success:(collection)->
        cb null, collection.toJSON()
      error:(collecion,response)->
        cb response

# class DBUtils
#   @StackMob = StackMob
#   @checkExistence = checkExistence = (data, collection, cb)->
#     query = new StackMob.Collection.Query()
#     for k,v of data
#       query.equals(k,v)
#     collection.query query, 
#       success:(collection)->
#         result = collection.toJSON()
#         if result.length > 0
#           cb result
#         else
#           cb null
#       error: (model, response)->
#         cb null
#   @getID = getID = (model, collection, cb) ->
#     checkExistence model.toJSON(), collection, (ids)->
#       if ids
#         if ids?.length?
#           cb ids[0]
#         else
#           cb ids
#       else
#         model.create
#           success:(model)->
#             cb model.toJSON()
#           error:(model, response)-> 
#             cb response
#   @getHARQID = (data, cb) ->
#     getID new HARQ(data), new HARQs(), cb
#   @getARQID = (data, cb) ->
#     getID new ARQ(data), new ARQs(), cb
#   @getPowerID = (data, cb) ->
#     getID new Power(data), new Powers(), cb
#   @getULID = (data, cb) ->
#     getID new ULProf(data), new ULProfs(), cb
#   @getDLID = (data, cb) ->
#     getID new DLProf(data), new DLProfs(), cb
#   @getUftpConfID = (data, cb) ->
#     getID new UftpConf(data), new UftpConfs(), cb
#   @getIperfConfID = (data, cb) ->
#     getID new IperfConf(data), new IperfConfs(), cb
#   @getNCConfID = (data, cb) ->
#     getID new NCConf(data), new NCConfs(), cb
#   @getIperfID = (data, cb) ->
#     getID new Iperf(data), new Iperfs(), cb
#   @getUftpID = (data, cb) ->
#     getID new Uftp(data), new Uftps(), cb
# 
# 
#   id = (item) ->
#     for k,y of item
#       if k.match "_id"
#         return y
# 
#   @saveUL = (cb)->
#     Base.getUL (xml) ->
#       parser = new xml2js.Parser()
#       parser.parseString xml, (err,result)->
#         toSave = result.Ulprofile
#         for k,v of toSave
#           toSave[k] = toSave[k].value
#         console.log toSave
#         DBUtils.getULID toSave, (item)->
#           cb id(item)
#   
#   @saveDL = (cb)->
#     Base.getDL (xml) ->
#       parser = new xml2js.Parser()
#       parser.parseString xml, (err,result)->
#         toSave = result.Dlprofile
#         for k,v of toSave
#           toSave[k] = toSave[k].value
#         console.log toSave
#         DBUtils.getDLID toSave, (item)->
#           cb id(item)
#           
#   @saveRF = (cb)->
#     # Base.getRF (xml) ->
#     #    parser = new xml2js.Parser()
#     #    parser.parseString xml, (err,result)->
#     #      toSave = result.Harq
#     #      for k,v of toSave
#     #        toSave[k] = toSave[k].value
#     #      console.log toSave
#     #      DBUtils.getULID toSave, (item)->
#     #        cb item.id
#                   
#   @savePower = (cb)->
#     Base.getPower (xml) ->
#       parser = new xml2js.Parser()
#       parser.parseString xml, (err,result)->
#         toSave = result.BaseStation
#         toSave.bs_tx_power = toSave.bs_tx_power.bs_tx_power
#         console.log toSave
#         DBUtils.getPowerID toSave, (item)->
#           cb item.id
#                   
#   @saveHARQ = (cb)->
#     Base.getHARQ (xml) ->
#       parser = new xml2js.Parser()
#       parser.parseString xml, (err,result)->
#         toSave = result.Harq
#         for k,v of toSave
#           toSave[k] = toSave[k].value
#         console.log toSave
#         DBUtils.getHARQID toSave, (item)->
#           cb item.id
#   @saveARQ = (cb)->
#     Base.getARQ (xml) ->
#       parser = new xml2js.Parser()
#       parser.parseString xml, (err,result)->
#         toSave = result.Arq
#         for k,v of toSave
#           toSave[k] = toSave[k].value
#         console.log toSave
#         DBUtils.getARQID toSave, (item)->
#           cb item.id
#   
#   @saveUftpConf = (toSave, cb)->
#     DBUtils.getUftpConfID toSave, (item)->
#       cb item.id
#   @saveIperfConf = (toSave, cb)->
#     DBUtils.getIperfConfID toSave, (item)->
#       cb item.id    
#   @saveNCConf = (toSave, cb)->
#     #github commit hash
#     DBUtils.getNCConfID toSave, (item)->
#       cb item.id
#           
#   @saveIperf = (toSave, cb)->
#     DBUtils.getIperfID toSave, (item)->
#       cb item.id
#   @saveUftp = (toSave, cb)->
#     DBUtils.getUftpID toSave, (item)->
#       cb item.id
# 
#   @getExperiments = (start,end,cb)->
#     exps = new Experiments()
#     query = new StackMob.Collection.Query()
#     query.setRange(start,end)
#     exps.query query,
#       success:(collection)->
#         cb collection.toJSON()
#       error:(collecion,response)->
#         cb null
# 
#   @saveExperiment = (data)->
#     exp = new Experiment(data)
#     exp.create
#       success:(model)->
#         cb model
#       error: (model, response)->
#         cb null
#        
  flushDB: (schemaName, cb)->
    Model =  StackMob.Model.extend(schemaName: schemaName)
    Collection =  StackMob.Collection.extend(model: Model)
    objs = new Collection()
    objs.fetch
      success: (collection)->
        for model in collection.toJSON()
          obj = new Model(model)
          obj.destroy()
         
module.exports = DBUtils


# # DBUtils.flushDB ->
# 

# db = new DBUtils
#   urlRoot: "http://api.mob1.stackmob.com/"
#   consumerKey: "a98bb1a2-13ae-4ff0-9ec8-16b41cd36e59"
#   consumerSecret: "2462c3ac-af0f-44ac-b63e-9064092bedc7"
#   appName: "orbit"
#   version: 0
#   dev: true
# 
# db.getExperiments ->
#   console.log arguments

# db.flushDB "arq_conf"
# db.flushDB "config"
# db.flushDB "experiment"
# db.flushDB "harq_conf"
# db.flushDB "iperf_conf"
# db.flushDB "iperf_result"
# db.flushDB "link_status"
# db.flushDB "nc_conf"
# db.flushDB "orbit_conf"
# db.flushDB "phys_conf"
# db.flushDB "uftp_conf"
# db.flushDB "uftp_result"

# db.updateExperiment 
#   experiment_id: '62258411f8544728b18a4505af4d0bee'
#   stdlog: 'Not Implemented'
#   stderr: 'Not Implemented'
#   uftp_result: 'bbb9e537bd8d4749ab080df3b7339012'
#   link_status: 'eda034156822410e9e5a3bf3e0d16c9f'
#   iperf_result: ['650e2b32185a4194ae5ab870bd4668c5','0e75f9d8c69a475992b3f58d99c69f5c'] 
# , ->
#   console.log arguments

# db.saveIperfResult [
#   timestamp: 20120327185301
#   id: 3
#   interval: "5.5-6.0"
#   transfer_bytes: 315000
#   bandwidth_bps: 5040000
#   jitter_ms: 2.418
#   lost: 0
#   total: 225
#   percent: 0
# ,
#   timestamp: 20120327185301
#   id: 3
#   interval: "5.5-6.0"
#   transfer_bytes: 315000
#   bandwidth_bps: 5040000
#   jitter_ms: 2.418
#   lost: 0
#   total: 225
#   percent: 0
# ,
#   timestamp: 20120327185301
#   id: 3
#   interval: "5.5-6.0"
#   transfer_bytes: 315000
#   bandwidth_bps: 5040000
#   jitter_ms: 1e-7
#   lost: 0
#   total: 225
#   percent: 0
# ], (err,result)->
#   console.log arguments