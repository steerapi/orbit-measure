Utils = require './utils'
SSHSession = require './session'
wimaxrf = "wimaxrf"
class BaseStation extends SSHSession
  checkBSUp: (cb)-> 
    ssh = @ssh
    ssh.exec "wget -q -O - 'http://#{wimaxrf}:5052/wimaxrf/harq'", (err, stdout)->
      console.log require("cli-color").red(stdout)
      if stdout.match /STATUS/
        cb null, true
      else
        cb null, false
    @
 
  setHARQ:(params, cb)->
    ssh = @ssh
    args = Utils.parseArgs params
    ssh.exec "wget -q -O - 'http://#{wimaxrf}:5052/wimaxrf/harq?#{args}'", (err, stdout)->
      console.log require("cli-color").red(stdout)
      if stdout.match /reboot/
        cb null, true
      else
        cb null, false
    @
    
  setARQ:(params, cb)->
    ssh = @ssh
    args = Utils.parseArgs params
    ssh.exec "wget -q -O - 'http://#{wimaxrf}:5052/wimaxrf/arq?#{args}'", (err, stdout)->
      console.log require("cli-color").red(stdout)
      if stdout.match /reboot/
        cb null, true
      else
        cb null, false
    @
    
  setPower:(power, cb)->
    ssh = @ssh
    ssh.exec "wget -q -O - 'http://#{wimaxrf}:5052/wimaxrf/set?bs_tx_power=#{power}'", (err, stdout)->
      console.log require("cli-color").red(stdout)
      if stdout.match /reboot/
        cb null, true
      else
        cb null, false
    @
  setUL:(mod, cb)->
    ssh = @ssh
    ssh.exec "wget -q -O - 'http://#{wimaxrf}:5052/wimaxrf/ulprofile?ulprof1=#{mod}&ulprof2=255&ulprof3=255&ulprof4=255&ulprof5=255&ulprof6=255&ulprof7=255&ulprof8=255&ulprof9=255&ulprof10=255'", (err, stdout)->
      console.log require("cli-color").red(stdout)
      if stdout.match /reboot/
        cb null, true
      else
        cb null, false
    @
  setDL:(mod, cb)->
    ssh = @ssh
    ssh.exec "wget -q -O - 'http://#{wimaxrf}:5052/wimaxrf/dlprofile?dlprof1=#{mod}&dlprof2=255&dlprof3=255&dlprof4=255&dlprof5=255&dlprof6=255&dlprof7=255&dlprof8=255&dlprof9=255&dlprof10=255?dlprof11=255&dlprof12=255'", (err, stdout)->
      console.log require("cli-color").red(stdout)
      if stdout.match /reboot/
        cb null, true
      else
        cb null, false
    @
  setRFAtt: (mobile, att, cb)->
    params = 
      portA: mobile[1]
      portB: 9
      att: att
    ssh = @ssh
    args = Utils.parseArgs params
    ssh.exec "wget -q -O - 'http://internal2dmz.orbit-lab.org:5052/instr/set?#{args}'", (err, stdout)->
      cb null, false
    @
  default: (cb)->
    ssh = @ssh
    ssh.exec "wget -q -O - http://#{wimaxrf}:5052/wimaxrf/defaults", (err, stdout)->
      cb null, true
    @
  restart: (cb)->
    ssh = @ssh
    ssh.exec "wget -q -O - http://#{wimaxrf}:5052/wimaxrf/restart", (err, stdout)->
      cb null, false
    @
module.exports = BaseStation

# base = new BaseStation("steerapi@console.sb4.orbit-lab.org")
# base.connect()
# base.checkBSUp ->
#   console.log arguments
# base.setHARQ 
#   enable: "false"
#   mimoul: false
#   uldelay: 2
#   dldelay: 1
#   pdusn: true
#   burst: 0x11
#   mret: 0
# ,->
#   console.log arguments
# base.setARQ 
#   enable: "false"
#   bloksize: 256
#   rtmorx: 1000
#   ackproctime: 0
#   wsize: 1024
#   blockltm: 5000
#   sltmo: 10000
#   rtmotx: 1000
#   rxpurgetmo: 5000
#   dlvrorder: true
#   txackdelay: 100
# ,->
#   console.log arguments
# base.setPower 41
# ,->
#   console.log arguments
# base.setUL
#   ulprof1: 13
# ,->
#   console.log arguments
# base.setDL
#   dlprof1: 13
# ,->
#   console.log arguments
# # base.setRFAtt 1, 0,->
# #   console.log arguments
# base.default ->
#   console.log arguments
# base.restart ->
#   console.log arguments